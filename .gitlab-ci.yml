---
include:
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/HEAD/templates/ci-fairy.yml'
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/HEAD/templates/alpine.yml'

stages:
  - container prep
  - sanity check
  - build
  - check

# https://docs.gitlab.com/ee/ci/yaml/README.html#switch-between-branch-pipelines-and-merge-request-pipelines
# if a MR is opened: run a detached MR pipeline
# if not, run a regular pipeline
workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH'

.common_variables:
  variables:
    FDO_UPSTREAM_REPO: centos-stream/rhel/src/kernel/documentation
    FDO_DISTRIBUTION_PACKAGES: make git python3 py3-jinja2 py3-yaml py3-jsonschema yamllint hugo asciidoctor
    # FDO_BASE_IMAGE and FDO_DISTRIBUTION_TAG define hugo version
    FDO_BASE_IMAGE: alpine:3.19
    FDO_DISTRIBUTION_TAG: '2024-05-20'

prep alpine container:
  stage: container prep
  extends:
    - .fdo.container-build@alpine
    - .common_variables

#
# Verify that commit messages are as expected, signed-off, etc.
#
check-commit:
  extends:
    - .fdo.ci-fairy
  stage: sanity check
  script:
    - ci-fairy check-commits --branch $CI_DEFAULT_BRANCH --signed-off-by --junit-xml=results.xml --textwidth=100
  except:
    - main@centos-stream/rhel/src/kernel/documentation
  variables:
    GIT_DEPTH: 100
  artifacts:
    reports:
      junit: results.xml

yaml lint:
  stage: sanity check
  extends:
    - .fdo.distribution-image@alpine
    - .common_variables
  rules:
    - if: '$CI_MERGE_REQUEST_DIFF_BASE_SHA'
  script:
    - yamllint .

gluser check:
  stage: sanity check
  extends:
    - .fdo.distribution-image@alpine
    - .common_variables
  rules:
    - if: '$CI_MERGE_REQUEST_DIFF_BASE_SHA'
  script:
    - |
      result=0
      for user in $(git diff $CI_MERGE_REQUEST_DIFF_BASE_SHA..$CI_COMMIT_SHA -- info/owners.yaml | sed -n 's/^+[ \t]*gluser:[ \t]*\(.*\)/\1/p'); do
        if ! wget -q -O - "https://gitlab.com/api/v4/users?username=$user" | grep -q username; then
          if [ $result = 0 ]; then
            echo '-----------------------------------------------------------------'
            echo
          fi
          echo " Error: user '$user' is not a valid GitLab user."
          result=1
        fi
      done
      if [ $result = 1 ]; then
        echo
        echo '-----------------------------------------------------------------'
      fi
      exit $result

.pages:
  stage: build
  extends:
    - .fdo.distribution-image@alpine
    - .common_variables
  variables:
    PAGES_BASE_URL: $CI_PAGES_URL
  script:
    - echo $CI_PAGES_URL
    - echo "Link to preview:"
    - echo "https://${CI_PROJECT_ROOT_NAMESPACE}.${CI_PAGES_DOMAIN}/${CI_PROJECT_PATH/$CI_PROJECT_ROOT_NAMESPACE/-}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html"

    - make docs-prepare
    - hugo --baseURL $PAGES_BASE_URL
  artifacts:
    paths:
      - public

pages CI check:
  extends:
    - .pages
  before_script:
    - export PROJECT_PATH_WITHOUT_ROOT=${CI_PROJECT_PATH/$CI_PROJECT_ROOT_NAMESPACE/-}
    - export PAGES_BASE_URL="https://${CI_PROJECT_ROOT_NAMESPACE}.${CI_PAGES_DOMAIN}/${PROJECT_PATH_WITHOUT_ROOT}/-/jobs/${CI_JOB_ID}/artifacts/public/"
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - when: on_success

check:
  stage: check
  extends:
    - .fdo.distribution-image@alpine
    - .common_variables
  script:
    - make

pages:
  extends:
    - .pages
  stage: check
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - when: never
